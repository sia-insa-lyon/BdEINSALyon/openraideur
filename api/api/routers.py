from rest_framework import routers
from openraideur.viewsets import RaceViewSet, LayerViewSet, WaypointViewSet, DayViewSet, YearViewSet, DisciplineViewSet, LevelViewSet


router = routers.DefaultRouter()
router.register(r'race', RaceViewSet)
router.register(r'discipline', DisciplineViewSet)
router.register(r'layer', LayerViewSet)
router.register(r'waypoint', WaypointViewSet)
router.register(r'day', DayViewSet)
router.register(r'year', YearViewSet)
router.register(r'level', LevelViewSet)