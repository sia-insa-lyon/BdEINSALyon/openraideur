from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from .serializers import CustomTokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView

# Create your views here.
def healthcheck(request):
    return HttpResponse(status=200)

def root(request):
    return redirect('/api/admin')


class CustomTokenObtainPairView(TokenObtainPairView):
    # Replace the serializer with your custom
    serializer_class = CustomTokenObtainPairSerializer