from rest_framework import renderers
from .models import Race, Waypoint, Layer

from xml.etree import ElementTree
import json


def GPXSerializeTrack(root, race):
    trk = ElementTree.SubElement(root, "trk")
    ElementTree.SubElement(trk, "name").text = race.name
    trk_link = ElementTree.SubElement(trk, "link", href="https://openraideur.raid.bde-insa-lyon.fr/race/" + str(race.id) + "/")
    ElementTree.SubElement(trk_link, "text").text = race.name + " - OpenRaideur"

    trk_seg = ElementTree.SubElement(trk, "trkseg")

    for p in race.track:
        trk_pt = ElementTree.SubElement(trk_seg, "trkpt", lat=str(p[0]), lon=str(p[1]))
        ElementTree.SubElement(trk_pt, "ele").text = str(p[2])


def GPXSerializeWaypoint(root, waypoint):
    wpt = ElementTree.SubElement(root, "wpt", lat=str(waypoint.lat), lon=str(waypoint.lon))
    ElementTree.SubElement(wpt, "name").text = waypoint.name
    wpt_link = ElementTree.SubElement(wpt, "link", href="https://openraideur.raid.bde-insa-lyon.fr/waypoint/" + str(waypoint.id) + "/")
    ElementTree.SubElement(wpt_link, "text").text = waypoint.name + " - OpenRaideur"


class GPXRenderer(renderers.BaseRenderer):
    media_type = 'application/gpx+xml'
    format = 'gpx'

    def render(self, data, media_type=None, renderer_context=None):
        if renderer_context['response'].status_code == 200:
            gpx = ElementTree.Element("gpx", creator="OpenRaideur")
            
            metadata = ElementTree.SubElement(gpx, "metadata")
            author = ElementTree.SubElement(metadata, "author")
            ElementTree.SubElement(author, "name").text = "Raid INSA Lyon - Orange"
            link = ElementTree.SubElement(metadata, "link", href="https://openraideur.raid.bde-insa-lyon.fr")
            ElementTree.SubElement(link, "text").text = "OpenRaideur"

            if isinstance(data, Race):
                GPXSerializeTrack(gpx, data)
            elif isinstance(data, Waypoint):
                GPXSerializeWaypoint(gpx, data)
            elif isinstance(data, Layer):
                for w in data.openraideur_waypoint_features.all():
                    GPXSerializeWaypoint(gpx, w)
                    
                for r in data.openraideur_race_features.all():
                    GPXSerializeTrack(gpx, r)

            return ElementTree.tostring(gpx)
        else:
            return "<xml>Code != 200</xml>"



class KMLRenderer(renderers.BaseRenderer):
    media_type = 'application/vnd.google-earth.kml+xml'
    format = 'kml'

    def render(self, data, media_type=None, renderer_context=None):
        if renderer_context['response'].status_code == 200:
            user_year = renderer_context['request'].user.userdetails.year
            day = data
            layers = Layer.objects.filter(day=day, year=user_year)

            kml = ElementTree.Element("kml", creator="OpenRaideur", xmlns="http://www.opengis.net/kml/2.2")
            doc = ElementTree.SubElement(kml, "Document")
            ElementTree.SubElement(doc, "name").text = day.name

            for layer in layers:
                l = ElementTree.SubElement(doc, "Folder")
                ElementTree.SubElement(l, "name").text = layer.name
                
                for waypoint in layer.openraideur_waypoint_features.all():
                    w = ElementTree.SubElement(l, "Placemark")
                    ElementTree.SubElement(w, "name").text = waypoint.name
                    ElementTree.SubElement(w, "description").text = waypoint.comment
                    w_pt = ElementTree.SubElement(w, "Point")
                    ElementTree.SubElement(w_pt, "altitudeMode").text = "absolute"
                    ElementTree.SubElement(w_pt, "coordinates").text = str(waypoint.lon) + ',' + str(waypoint.lat)

                for race in layer.openraideur_race_features.all():
                    r = ElementTree.SubElement(l, "Placemark")
                    ElementTree.SubElement(r, "name").text = race.name
                    ElementTree.SubElement(r, "description").text = race.comment
                    r_pts = ElementTree.SubElement(r, "LineString")
                    ElementTree.SubElement(r_pts, "altitudeMode").text = "absolute"
                    serialized_track = ""
                    for pt in race.track:
                        lat, lng, alt = pt
                        serialized_track = serialized_track + str(lng) + ',' + str(lat) + ',' + str(alt) + '\n'
                    ElementTree.SubElement(r_pts, "coordinates").text = serialized_track
        
            return ElementTree.tostring(kml)
        else:
            return "<kml>Code != 200</kml>"