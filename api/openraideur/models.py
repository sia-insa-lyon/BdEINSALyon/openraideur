import os

from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import User

from django.db.models.signals import post_delete
from django.dispatch import receiver

import requests
import json
import math

from uuid import uuid4

# Create your models here.

class UserDetails(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    year = models.ForeignKey('Year', on_delete=models.PROTECT, verbose_name='Edition')
    external_user = models.BooleanField(verbose_name="Utilisateur externe au BdE", default=False)
    allowed_layers = models.ManyToManyField("Layer", verbose_name="Couches autorisées", help_text="Si l'utilisateur est interne au BdE, vous pouvez ignorer cette partie. Sinon, ajoutez les calques que l'utilisateur externe pourra consulter.", blank=True)

class Year(models.Model):
    year = models.PositiveSmallIntegerField(verbose_name='Année')

    def __str__(self):
        return str(self.year)

class Day(models.Model):
    name = models.CharField(verbose_name='Nom du jour', max_length=30)
    short_name = models.CharField(verbose_name='Abbréviation', max_length=2)
    icon = models.CharField(max_length=40, verbose_name="Icône", null=True, blank=True)
    order = models.PositiveSmallIntegerField(verbose_name="Ordre", default=0)

    def __str__(self):
        return str(self.name)

    class Meta:
        ordering = ['order', 'name']
        

class Discipline(models.Model):
    name = models.CharField(max_length=30, verbose_name='Nom de la discipline')
    icon = models.CharField(max_length=40, verbose_name="Icône", null=True, blank=True)

    
    def __str__(self):
        return self.name


class Level(models.Model):
    name = models.CharField(max_length=10, verbose_name='Nom du niveau de difficulté')

    def __str__(self):
        return self.name

class Layer(models.Model):
    name = models.CharField(max_length=50, verbose_name="Nom")
    year = models.ForeignKey('Year', on_delete=models.CASCADE, verbose_name='Année')
    day = models.ForeignKey('Day', on_delete=models.CASCADE, verbose_name='Jour')
    active = models.BooleanField(default=False, verbose_name="Affiché par défaut")

    def __str__(self):
        return self.name + " (" + str(self.year) + "/" + str(self.day) + ")"


class Feature(models.Model):
    name = models.CharField(max_length=50, verbose_name="Nom")
    layer = models.ForeignKey('Layer', on_delete=models.CASCADE, verbose_name='Couche', related_name="%(app_label)s_%(class)s_features")
    color = models.CharField(max_length=7, verbose_name="Couleur", default="#FF0000")
    comment = models.TextField(verbose_name="Commentaire", blank=True, default="")

    class Meta:
        abstract = True
        ordering = ['name']

    def __str__(self):
        return self.name

    def get_city_name(self, longitude, latitude):   
        response = requests.get("https://geo.api.gouv.fr/communes?fields=nom&lon={lon}&lat={lat}".format(lon=longitude, lat=latitude))
        if (response.status_code != 200):
            raise Exception("Erreur requete geo.api.gouv.fr !")
        return json.loads(response.text)[0]['nom']


class Waypoint(Feature):
    def upload_path_and_rename(instance, filename):
        ext = filename.split('.')[-1]
        return os.path.join("waypoints", 'waypoint_{}_{}.{}'.format(instance.pk, uuid4().hex, ext))
            
    lat = models.FloatField(verbose_name='Latitude')
    lon = models.FloatField(verbose_name='Longitude')
    icon = models.CharField(max_length=40, verbose_name="Icône", default="dot-circle")
    img = models.FileField(verbose_name="Image", upload_to=upload_path_and_rename, blank=True, null=True)
    city = models.CharField(max_length=40, verbose_name="Ville", null=True, blank=True)
    

    def save(self, *args, **kwargs):
        if getattr(self, '_lat_changed', True) or getattr(self, '_lon_changed', True):
            self.city = self.get_city_name(self.lon, self.lat)

        try:
            this = Waypoint.objects.get(id=self.id)
            if this.img != self.img:
                this.img.delete(save=False)
        except: pass
        super(Waypoint, self).save(*args, **kwargs)

@receiver(post_delete, sender=Waypoint)
def waypoint_delete(sender, instance, **kwargs):
    if instance.img:
        instance.img.delete(False)
    

class Race(Feature):
    discipline = models.ForeignKey('Discipline', on_delete=models.CASCADE, verbose_name="Discipline", null=True, blank=True)
    level = models.ForeignKey('Level', on_delete=models.CASCADE, verbose_name='Niveau', null=True, blank=True)
    previous_race = models.ForeignKey('self', verbose_name="Epreuve précédente", on_delete=models.CASCADE, related_name='next_race', blank=True, null=True)
    track = ArrayField(ArrayField(models.FloatField(null=True)), verbose_name='Coordonnées des points du parcours', null=True, blank=True)
    start_city = models.CharField(max_length=40, verbose_name="Ville de départ", null=True, blank=True)
    end_city = models.CharField(max_length=40, verbose_name="Ville d'arrivée", null=True, blank=True)
    distance = models.DecimalField(max_digits=5, decimal_places=2,verbose_name="Distance (en km)", null=True, blank=True)
    max_elevation = models.SmallIntegerField(verbose_name="Altitude maximale (en m)", null=True, blank=True)
    positive_elevation = models.SmallIntegerField(verbose_name="D+ (en m)", null=True, blank=True)
    negative_elevation = models.SmallIntegerField(verbose_name="D- (en m)", null=True, blank=True)
    
    
    def save(self, *args, **kwargs):
        if getattr(self, '_track_changed', True):
            if self.track:
                self.convert_track_to_3d()
                self.compute_track_stats()
                self.find_edge_cities()
        super(Race, self).save(*args, **kwargs)

    
    def compute_track_stats(self):
        track_3D = self.track
        self.max_elevation = round(max([coord[2] for coord in track_3D]))
        
        computed_distance = 0
        computed_positive_elevation = 0
        computed_negative_elevation = 0
        for i in range(len(track_3D) - 1):
            origin_coord = track_3D[i][:2]
            origin_elev = track_3D[i][2]
            destination_coord = track_3D[i+1][:2]
            destination_elev = track_3D[i+1][2]
            computed_distance = computed_distance + self.distance_from_coords(origin_coord, destination_coord)
            elevation_difference = destination_elev - origin_elev
            if elevation_difference > 0:
                computed_positive_elevation = computed_positive_elevation + elevation_difference
            else:
                computed_negative_elevation = computed_negative_elevation + elevation_difference
        self.distance = computed_distance
        self.positive_elevation = computed_positive_elevation
        self.negative_elevation = computed_negative_elevation
    

    def find_edge_cities(self):
        start_point = {'lat': self.track[0][0], 'lon': self.track[0][1]}
        end_point = {'lat': self.track[-1:][0][0], 'lon': self.track[-1:][0][1]}

        self.start_city = self.get_city_name(start_point['lon'], start_point['lat'])
        self.end_city = self.get_city_name(end_point['lon'], end_point['lat'])


    #[lat, long], [lat, long]
    def distance_from_coords(self, origin, destination):
        lat1, lon1 = origin
        lat2, lon2 = destination
        radius = 6371 # km

        dlat = math.radians(lat2-lat1)
        dlon = math.radians(lon2-lon1)
        a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
            * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        d = radius * c

        return d


    def convert_track_to_3d(self):
        coordinates_to_lookup = []
        indices_to_lookup = []
        
        for i in range(len(self.track)):
            if len(self.track[i]) == 2:
                coordinates_to_lookup.append(self.track[i])
                indices_to_lookup.append(i)

        if len(coordinates_to_lookup) > 0:   
            elevations = [] 
            req_iter = 0
            coords_to_lookup_per_iter = 200
            while req_iter * coords_to_lookup_per_iter < len(coordinates_to_lookup):
                req_iter += 1

                lat_str = '|'.join([str(coordinates_to_lookup[i][0]) for i in range((req_iter-1) * coords_to_lookup_per_iter, min(len(coordinates_to_lookup), req_iter * coords_to_lookup_per_iter))])    
                lon_str = '|'.join([str(coordinates_to_lookup[i][1]) for i in range((req_iter-1) * coords_to_lookup_per_iter, min(len(coordinates_to_lookup), req_iter * coords_to_lookup_per_iter))]) 
                url = "https://data.geopf.fr/altimetrie/1.0/calcul/alti/rest/elevation.json?lon={longitudes}&lat={latitudes}&zonly=true&resource=ign_rge_alti_wld".format(api_key="essentiels", longitudes=lon_str, latitudes=lat_str)
                print(url)
                response = requests.get(url)

                if (response.status_code != 200):
                    raise Exception("Erreur requete Geoportail !")
                
                elevations += json.loads(response.content)['elevations']
                
            for i in range(len(indices_to_lookup)):
                coordinates_to_lookup[i].append(elevations[i])
                self.track[indices_to_lookup[i]] = coordinates_to_lookup[i]
