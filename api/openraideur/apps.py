from django.apps import AppConfig


class OpenraideurConfig(AppConfig):
    name = 'openraideur'
