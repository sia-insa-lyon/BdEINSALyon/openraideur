from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Race, Level, Discipline, Year, Waypoint, Layer, Day, UserDetails

# Register your models here.


@admin.register(Race)
class RaceAdmin(admin.ModelAdmin):
    readonly_fields=('start_city', 'end_city', 'distance', 'max_elevation', 'positive_elevation', 'negative_elevation', 'track',)
    list_display = ('name', 'get_year', 'get_day', 'get_layer_name', 'start_city', 'end_city', 'distance', 'positive_elevation', 'negative_elevation', 'id')
    list_filter = ('layer__year', 'layer__day')

    def get_year(self, obj):
        return obj.layer.year
    get_year.short_description = 'Année'

    def get_day(self, obj):
        return obj.layer.day
    get_day.short_description = 'Jour'

    def get_layer_name(self, obj):
        return obj.layer.name
    get_layer_name.short_description = 'Couche'


@admin.register(Waypoint)
class WaypointAdmin(admin.ModelAdmin):
    readonly_fields=('lat', 'lon', 'city')
    list_display = ('name', 'get_year', 'get_day', 'get_layer_name', 'city', 'id')
    list_filter = ('layer__year', 'layer__day')

    def get_year(self, obj):
        return obj.layer.year
    get_year.short_description = 'Année'

    def get_day(self, obj):
        return obj.layer.day
    get_day.short_description = 'Jour'

    def get_layer_name(self, obj):
        return obj.layer.name
    get_layer_name.short_description = 'Couche'


@admin.register(Layer)
class LayerAdmin(admin.ModelAdmin):
    list_display = ('name', 'year', 'day', 'id')
    list_filter = ('year', 'day')


@admin.register(Day)
class DayAdmin(admin.ModelAdmin):
    list_display = ('name', 'order', 'id')
    list_editable = ('order',)


class UserDetailsInline(admin.StackedInline):
    model = UserDetails
    can_delete = False
    filter_horizontal = ('allowed_layers',)
    verbose_name_plural = "Informations"
    

class UserAdmin(UserAdmin):
    inlines = (UserDetailsInline, )

admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(Level)
admin.site.register(Discipline)
admin.site.register(Year)
