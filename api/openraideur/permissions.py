from rest_framework.permissions import BasePermission

class FeaturePermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user.is_authenticated:
            if request.user.userdetails.external_user:                        # compte d'un utilisateru externe, il n'a accès qu'à des calques spécifiques
                return obj.layer in request.user.userdetails.allowed_layers.all()
            return obj.layer.year == request.user.userdetails.year            # compte interne, accès si l'édition matche
        return False

class LayerPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user.is_authenticated:
            if request.user.userdetails.external_user:               # compte d'un utilisateru externe, il n'a accès qu'à des calques spécifiques
                return obj in request.user.userdetails.allowed_layers.all()                                  
            return obj.year == request.user.userdetails.year         # compte interne, accès si l'édition matche
        return False