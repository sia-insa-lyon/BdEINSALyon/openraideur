# Generated by Django 3.0.5 on 2020-11-22 14:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('openraideur', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='waypoint',
            name='img',
            field=models.FileField(blank=True, default='default.jpg', upload_to='waypoints'),
        ),
    ]
