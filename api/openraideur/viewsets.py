from rest_framework import viewsets
from .models import Race, Layer, Waypoint, Day, Year, Discipline, Level
from .serializers import RaceSerializer, LayerSerializer, LayerCreateSerializer, WaypointSerializer, DaySerializer, YearSerializer, DisciplineSerializer, LevelSerializer
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from .permissions import FeaturePermission, LayerPermission
from rest_framework.filters import BaseFilterBackend

from openraideur import renderers
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser

class FeatureFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if request.user.userdetails.external_user:
            return queryset.filter(layer__id__in=request.user.userdetails.allowed_layers.all())
        return queryset.filter(layer__year__year=request.user.userdetails.year.year)

class LayerFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if request.user.userdetails.external_user:
            return queryset.filter(id__in=request.user.userdetails.allowed_layers.all())
        return queryset.filter(year__year=request.user.userdetails.year.year)

class DayFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if request.user.userdetails.external_user:
            return queryset.filter(id__in=request.user.userdetails.allowed_layers.values('day'))
        return queryset.all()                               # a corriger pour que les années futures ne voient pas les journées des éditions précédentes

class RaceViewSet(viewsets.ModelViewSet):
    queryset = Race.objects.all()
    serializer_class = RaceSerializer
    permission_classes = (FeaturePermission, DjangoModelPermissions)
    filter_backends = (FeatureFilter, )

    @action(detail=True, renderer_classes=[renderers.GPXRenderer])
    def gpx(self, request, *args, **kwargs):
        obj = self.get_object()
        return Response(obj)

class WaypointViewSet(viewsets.ModelViewSet):
    queryset = Waypoint.objects.all()
    serializer_class = WaypointSerializer
    permission_classes = (FeaturePermission, DjangoModelPermissions)
    filter_backends = (FeatureFilter, )

    @action(detail=True, renderer_classes=[renderers.GPXRenderer])
    def gpx(self, request, *args, **kwargs):
        obj = self.get_object()
        return Response(obj)

    @action(detail=True, methods=['put', 'delete', 'get'], parser_classes=(MultiPartParser,))
    def img(self, request, pk=None):
        waypoint = self.get_object()
        if request.method == 'PUT':
            try:
                file_obj = request.data['img']
                waypoint.img = file_obj
                waypoint.save()
                res = Response(status=201)
                res['Location'] = waypoint.img.url
                return res
            except KeyError:
                return Response(status=400)
        elif request.method == 'DELETE':
            waypoint.img = None
            waypoint.save()
            return Response(status=204)
        elif request.method == "GET":
            if waypoint.img:
                response = Response('')
                response['X-Accel-Redirect'] = waypoint.img.url.replace('media', 'protected')
                response['Content-Type'] = ''
                return response
            else:
                return Response(status=404)
            

class DayViewSet(viewsets.ModelViewSet):
    queryset = Day.objects.all().order_by('order', 'name')
    serializer_class = DaySerializer
    permission_classes = (DjangoModelPermissions, )
    filter_backends = (DayFilter, )

    @action(detail=True, renderer_classes=[renderers.KMLRenderer])
    def kml(self, request, *args, **kwargs):
        obj = self.get_object()
        return Response(obj)

class YearViewSet(viewsets.ModelViewSet):
    queryset = Year.objects.all()
    serializer_class = YearSerializer
    permission_classes = (DjangoModelPermissions, )

class DisciplineViewSet(viewsets.ModelViewSet):
    queryset = Discipline.objects.all()
    serializer_class = DisciplineSerializer
    permission_classes = (DjangoModelPermissions, )

class LevelViewSet(viewsets.ModelViewSet):
    queryset = Level.objects.all()
    serializer_class = LevelSerializer
    permission_classes = (DjangoModelPermissions, )


class LayerViewSet(viewsets.ModelViewSet):
    queryset = Layer.objects.all()
    permission_classes = (DjangoModelPermissions, LayerPermission)
    filter_backends = (LayerFilter, )

    def get_serializer_class(self):
        if self.action == 'create':
            return LayerCreateSerializer
        return LayerSerializer
    
    def perform_create(self, serializer):
        serializer.save(year=self.request.user.userdetails.year)

    @action(detail=True, renderer_classes=[renderers.GPXRenderer])
    def gpx(self, request, *args, **kwargs):
        obj = self.get_object()
        return Response(obj)