from rest_framework import serializers
from drf_dynamic_fields import DynamicFieldsMixin
from .models import Race, Layer, Waypoint, Day, Year, Discipline, Level
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

class DisciplineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Discipline
        fields = '__all__'

class LevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Level
        fields = '__all__'

class RaceSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = Race
        fields = '__all__'

    def to_representation(self, obj):
        rep = super(RaceSerializer, self).to_representation(obj)
        request = self.context.get('request', None)

        if request.user.userdetails.external_user:
            if 'comment' in rep:
                rep.pop('comment')
        return rep

class LightRaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Race
        exclude = ('track', )

    def to_representation(self, obj):
        rep = super(LightRaceSerializer, self).to_representation(obj)
        request = self.context.get('request', None)

        if request.user.userdetails.external_user:
            rep.pop('comment')
        return rep

class WaypointSerializer(serializers.ModelSerializer):
    class Meta:
        model = Waypoint
        fields = '__all__'

    img = serializers.SerializerMethodField('has_img')

    def has_img(self, obj):
        if obj.img:
            return True
        else:
            return False

    def to_representation(self, obj):
        rep = super(WaypointSerializer, self).to_representation(obj)
        request = self.context.get('request', None)

        if request.user.userdetails.external_user:
            rep.pop('comment')
        return rep

class DaySerializer(serializers.ModelSerializer):
    class Meta:
        model = Day
        fields = '__all__'

class YearSerializer(serializers.ModelSerializer):
    class Meta:
        model = Year
        fields = '__all__'


class LayerSerializer(serializers.ModelSerializer):
    openraideur_race_features = LightRaceSerializer(many=True, read_only=True)
    openraideur_waypoint_features = WaypointSerializer(many=True, read_only=True)
    year = YearSerializer()
    day = DaySerializer()

    class Meta:
        model = Layer
        fields = '__all__'

class LayerCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Layer
        exclude = ('year', )
    
    def to_representation(self, instance):
        serializer = LayerSerializer(instance)
        return serializer.data




class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        # The default result (access/refresh tokens)
        data = super(CustomTokenObtainPairSerializer, self).validate(attrs)
        # Custom data you want to include
        data.update({'username': self.user.username})
        data.update({'year': self.user.userdetails.year.year})
        data.update({'external_user': self.user.userdetails.external_user})
        return data