const API_CACHE_NAME = "api";
const DOCUMENT_CACHE_NAME = "document";
const STATIC_CACHE_NAME = "static";

const getResponseFromCache = (cacheName, request) => {
    return caches.open(cacheName).then(cache => {
        return cache.match(request);
    });
};

const setResponseCache = (cacheName, request, response) => {
    return caches.open(cacheName).then(cache => {
        return cache
            .put(request, response.clone())
            .then(response => {
                cache.keys();
                return response;
            })
            .catch(error => console.log(error));
    });
};

const getResponseFromNetworkFirst = (cacheName, request) => {
    return fetch(request)
        .then(response => {
            setResponseCache(cacheName, request, response.clone());
            return response;
        })
        .catch(error => {
            return getResponseFromCache(cacheName, request).then(response => {
                return response;
            })
        })
};

self.addEventListener("install", event => {
    self.skipWaiting();
});

self.addEventListener("fetch", event => {
    const requestUrl = new URL(event.request.url);
    if (requestUrl.pathname.match(/^\/api\/(day|discipline|layer|level|race|waypoint|year)\//) != null) {
        event.respondWith(getResponseFromNetworkFirst(API_CACHE_NAME, event.request));
    }
    else if (requestUrl.pathname.match(/(^\/$|^\/gps$)/) != null) {
        event.respondWith(getResponseFromNetworkFirst(DOCUMENT_CACHE_NAME, event.request));
    }
    else if (requestUrl.pathname.match(/^\/((js|css)\/(app|chunk-vendors)(\.[a-z0-9]{8})?\.(js|css)|favicon\.png)$/) != null) {
        event.respondWith(getResponseFromNetworkFirst(STATIC_CACHE_NAME, event.request));
    }
});