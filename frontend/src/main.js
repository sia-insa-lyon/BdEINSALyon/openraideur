import Vue from 'vue'
import App from './App.vue'
import routes from './routes';

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import axios from "axios";

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

import "./registerServiceWorker";

// Install BootstrapVue
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

import VueFontAwesomePicker from "vfa-picker";
Vue.use(VueFontAwesomePicker);


Vue.config.productionTip = false


// Add a request interceptor
axios.interceptors.request.use(
  config => {
      if (config.url !== 'api/auth/' || config.url !== 'api/auth/refresh/') {
        const token = localStorage.getItem('access_token');
        if (token) {
            config.headers['Authorization'] = 'Bearer ' + token;
        }
        else {
          router.push('/login?next=' + router.history._startLocation);
        }
      }
      return config;
  },
  error => {
      Promise.reject(error)
  });

//Add a response interceptor

axios.interceptors.response.use((response) => {
  return response
}, async function (error) {
  const originalRequest = error.config;
  const refreshToken = localStorage.getItem('refresh_token');

  if (error.response.status === 401 && (originalRequest.url === 'api/auth/refresh/' || !refreshToken)) {
      router.push('/login?next=' + router.history._startLocation);
      return Promise.reject(error);
  }

  if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;
      const res = await axios.post('api/auth/refresh/', {
        "refresh": refreshToken
      });
    if (res.status === 200) {
      localStorage.setItem('access_token', res.data.access);
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return axios(originalRequest);
    }
  }
  return Promise.reject(error);
});

const router = new VueRouter({routes, mode: 'history'});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
