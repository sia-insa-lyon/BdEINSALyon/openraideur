if ("serviceWorker" in navigator) {
    navigator.serviceWorker
        .register("/service-worker.js")
        .catch(error => {
            console.error(
                "Service Worker: Error on register",
                error
            );
        });
}