import Map from './components/Map.vue';
import Login from './components/Login.vue'
import Gps from './components/Gps.vue'

const routes = [
    { path: '/', component: Map },
    { path: '/gps', component: Gps },
    { path: '/login', component: Login },
];

export default routes;